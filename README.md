# Hoja03_Markdown_02


1. Crea un repositorio en GitLab o GitHub llamado TUNOMBRE_markdown
![](img/git_1.PNG "git_1")
![](img/git_2.PNG "git_2")

2. Clona el repositorio en local

![](img/git_3.PNG "git_3")

3. Crea en tu repositorio local un documento README.md. Nota: en este documento tendrás que ir poniendo los comandos que has tenido que utilizar durante todos los ejercicios y las explicaciones y capturas de pantalla queconsideres necesarias

![](img/git_4.PNG "git_4")
![](img/git_5.PNG "git_5")

4. Añadir al README.md los comandos utilizados hasta ahora y hacer un commit inicial con el mensaje “Primer commit de TUNOMBRE”.

![](img/git_6.PNG "git_6")

5. Sube los cambios al repositorio remoto.

![](img/git_7.PNG "git_7")

6. Crear en el repositorio local un fichero llamado privado.txt. Crear en el repositorio local una carpeta llamada privada.

![](img/git_8.PNG "git_8")

7. Realizar los cambios oportunos para que tanto el archivo como la carpeta sean ignorados por git.

![](img/git_9.PNG "git_9")
![](img/git_10.PNG "git_10")

8. Añade el fichero tunombre.md en el que se muestre un listado de los módulos en los que estás matriculado.

![](img/git_11.PNG "git_11")

9. Crea un tag llamado v0.1

![](img/git_12.PNG "git_12")

10. Sube los cambios al repositorio remoto

![](img/git_13.PNG "git_13")

11. Por último, crea una tabla en el documento anterior en el que se muestre el nombre de 2 compañeros y su enlace al repositorio en GitLab o GitHub.

|    Nombre    | Link 	     |
| :------------| :-----------|
|Paula         |[Enlace][1]|
|Karla         |[Enlace][2]|

[1]: https://gitlab.com/PRGGR/paula_markdown
[2]: https://gitlab.com/KarlaNC/karla_markdown

# Hoja03_Markdown_03

###### A partir de la tarea anterior vamos a profundizar más en el uso de Git:
1. Creación de ramas:
	a) Crea la rama rama-TUNOMBRE

	![](img/git_14.PNG "git_14")

	b) Posiciona tu carpeta de trabajo en esta rama

	![](img/git_15.PNG "git_15")

2. Añade un fichero y crea la rama remota
	c) Crea un fichero llamado daw.md con únicamente una cabecera DESARROLLO DE APLICACIONES WEB

	![](img/git_16.PNG "git_16")

	d) Haz un commit con el mensaje “Añadiendo el archivo daw.md en la rama-TUNOMBRE”

	![](img/git_17.PNG "git_17")

	e) Sube los cambios al repositorio remoto. NOTA: date cuenta que ahora se deberá hacer el comando git push origin rama-TUNOMBRE

	![](img/git_18.PNG "git_18")

3. Haz un merge directo
	f) Posiciónate en la rama master

	![](img/git_19.PNG "git_19")

	g) Haz un merge de la rama-TUNOMBRE en la rama master

	![](img/git_20.PNG "git_20")

4. Haz un merge con conflicto
	h) En la rama master añade al fichero daw.md una tabla en la que muestres los módulos del primer curso de DAW.

	![](img/git_21.PNG "git_21")

	i) Añade los archivos y haz un commit con el mensaje “Añadida tabla DAW1”

	![](img/git_22.PNG "git_22")

	j) Posiciónate ahora en la rama-TUNOMBRE

	![](img/git_23.PNG "git_23")

	k) Escribe en el fichero daw.md otra tabla con los módulos del segundo curso de DAW

	![](img/git_24.PNG "git_24")

	l) Añade los archivos y haz un commit con el mensaje “Añadida tabla DAW2”

	![](img/git_25.PNG "git_25")

	m) Posiciónate otra vez en master y haz un merge con la rama-TUNOMBRE

	![](img/git_26.PNG "git_26")

	![](img/git_27.PNG "git_27")

5. Arreglo del conflicto
	n) Arregla el conflicto editando el fichero daw.md y haz un commit con el mensaje “Finalizado el conflicto de daw.md”

	![](img/git_28.PNG "git_28")

6. Tag y borrar la rama
	o) Crea un tag llamado v0.2

	![](img/git_29.PNG "git_29")

	p) Borra la rama-TUNOMBRE

	![](img/git_30.PNG "git_30")

7. Documenta todo y finaliza el ejercicio
	



